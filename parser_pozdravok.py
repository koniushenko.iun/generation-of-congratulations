import requests as re
from bs4 import BeautifulSoup

storage = []

for i in range(100):
    try:
        response = re.get(f"https://pozdravok.com/pozdravleniya/den-rozhdeniya/proza-{i+1}.htm")
        if response.status_code != 200:
            continue
    except Exception as e:
        print(str(e))

    soup = BeautifulSoup(response.text, features="lxml")
    
    dv = soup.find("div", {"class":"content"})
    for each in dv:
        if each.find('p'):
            continue
        else:
            tx = each.text
            tx = tx.strip()
            if tx == "":
                continue
            storage.append(tx)
            
for item in storage:
    print(item)
