import requests as re
from bs4 import BeautifulSoup

for i in range(4):
    try:
        url = ""
        if i == 0:
            url = "https://moonzori.com/pozdravlenyya-s-dnem-rozhdenyya-v-proze/"
        else:
            url = f"https://moonzori.com/pozdravlenyya-s-dnem-rozhdenyya-v-proze/{i+1}/"
        response = re.get(url)
        if response.status_code != 200:
            continue
    except Exception as e:
        print(str(e))

    soup = BeautifulSoup(response.text, features="lxml")
    
    article = soup.find("div", {"class":"article-content entry-content"})
    ps = article.find_all("p")
    for each in ps:
    
        tx = each.text
        tx = tx.strip()
        tx = tx.strip("*")
        tx = tx.strip("1 2 3 4")
        tx = tx.strip("Также есть:")
        tx = tx.strip("Ещё поздравлений для женщины")
        tx = tx.strip("Музыкальное поздравлени")
        if tx == "":
            continue
        print(tx)
