import requests as re
from bs4 import BeautifulSoup

for i in range(10):
    try:
        url = ""
        if i == 0:
            url = "https://datki.net/pozdravleniya-s-dnem-rozhdeniya/krasivie/"
        else:
            url = f"https://datki.net/pozdravleniya-s-dnem-rozhdeniya/krasivie/page/{i+1}/"
        response = re.get(url)
        if response.status_code != 200:
            continue
    except Exception as e:
        print(str(e))

    soup = BeautifulSoup(response.text, features="lxml")
    
    articles = soup.find_all("article", {"class":"dn-entry-content"})
    for article in articles:

        dvs = article.find_all("div", {"class": "entry-summary"})
        for each in dvs:
        
            tx = each.text
            tx = tx.strip()
            if tx == "":
                continue
            tx = tx.rstrip("""
    Копировать
    Постоянная ссылка
    В закладки""")
            print(tx)
